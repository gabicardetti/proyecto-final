import * as jwt from "jsonwebtoken";

import { Request } from "express";
import { User } from "../types/User";
import config from "../config";



const jwtSecret: string = config.JWT_SECRET


interface PayloadDto {
  email?: string;
  userId: string
}

const buildJwt = (user: User): string => {
  const payload: PayloadDto = { email: user.email, userId: user.id };
  const token = jwt.sign(payload, jwtSecret, { expiresIn: "24h" });
  return token;
};

const getPayload = (token: string) => {
  const payload = jwt.decode(token, { json: true });
  return payload;
};

const verifyAndGetPayload = (token: string) => {
  if (!token) {
    return;
  }
  try {
    const jwtPayload = jwt.verify(token, jwtSecret);
    return jwtPayload;
  } catch (error) {
  }
};

const verify = (token: string, secret: string): boolean => {
  try {
    jwt.verify(token, secret);
  } catch (error) {
    return false;
  }
  return true;

};

const getJwtFromHeaders = (req: Request): string => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(" ")[0] === "Bearer"
  ) {
    return <string>req.headers.authorization.split(" ")[1];
  }
  return ""
};

export { buildJwt, getPayload, verifyAndGetPayload, getJwtFromHeaders, verify };
