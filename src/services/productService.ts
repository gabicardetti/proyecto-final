import { NewProductDto, Product, EditProductDto } from "../types/Product";
import ProductRepository from "../repositorys/productRepository"

const productRepository = new ProductRepository();

export const getAllProducts = async () => {
    const products = await productRepository.getAll();
    return products;
};

export const generateNewProduct = async (dto: NewProductDto) => {
    const product = productRepository.save(dto);
    return product;
};

export const getProductById = async (id: string): Promise<Product | undefined> => {
    const product = await productRepository.getById(id);
    return product;
};

export const deleteProductById = async (id: string) => {
    const product = await productRepository.deleteById(id)
    return product;
};

export const updateProductById = async (id: string, dto: EditProductDto) => {
    const product = await productRepository.getById(id);

    if(!product) return;

    if (dto.timestamp) product.timestamp = dto.timestamp;
    if (dto.name) product.name = dto.name;
    if (dto.description) product.description = dto.description;
    if (dto.code) product.code = dto.code;
    if (dto.photoUrl) product.photoUrl = dto.photoUrl;
    if (dto.price) product.price = dto.price;
    if (dto.stock) product.stock = dto.stock;
    
    return productRepository.updateById(id, product);;
};