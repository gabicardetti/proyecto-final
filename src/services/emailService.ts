import nodemailer from "nodemailer";
import SESTransport from "nodemailer/lib/ses-transport";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import config from "../config";
import logger from "../util/logger";

const admimEmail = "admin@admin.com"

const transporter = nodemailer.createTransport({
    host: config.EMAIL_HOST,
    port: config.EMAIL_PORT,
    auth: {
        user: config.EMAIL_AUTH.USER,
        pass: config.EMAIL_AUTH.PASS,
    }
});

const sendEmail = (subject: string, text: string, html: string, type: string, to?: string) => {
    let message = {
        from: "sender@email.com",
        to: to || admimEmail,
        subject,
        text,
        html
    };

    transporter.sendMail(message, (error: any, info: SESTransport.SentMessageInfo | SMTPTransport.SentMessageInfo) => {
        if (error) {
            logger.error("Error occurred");
            logger.error(error.message);
            return;
        }

        console.log("Message sent " + type);
        const infoMessage = new String(nodemailer.getTestMessageUrl(info));
        console.log(infoMessage);
    });
};

export const sendNewUserRegister = (name: string) => {
    const text = `El usuario ${name} se creo una cuenta correctamente`;
    const html = `<p> El usuario ${name} se creo una cuenta correctamente </p>`;
    const subject = "Registro exitoso";
    return sendEmail(subject, text, html, "Register message");
};

export const sendNewOrder = (orderId: string, email: string) => {
    const text = `Nueva orden creada exitosamente ID":${orderId}`;
    const html = `<p> Nueva orden creada exitosamente ID":${orderId}</p>`;
    const subject = "Orden generada";
    return sendEmail(subject, text, html, "New Order", email);
};