import { Twilio } from "twilio";
import config from "../config";

const accountSid: string = config.TWILIO_AUTH.ACCOUNT_SID;
const authToken: string = config.TWILIO_AUTH.AUTH_TOKEN;
const to = "+";
const from = config.TWILIO_AUTH.FROM;

export const sendMessageBySms = async (message: string) => {
    if (!config.SMS_SERVICE_ENABLE) return;

    const client = new Twilio(accountSid, authToken);
    try {
        const result = await client.messages.create({
            body: message,
            from,
            to,
        });
    } catch (e) {
        console.log(e);
    }
};
