import { createNewUser, getUserByUsername } from "./userService";
import bcript from "bcryptjs"
import { buildJwt } from "./jwtService";

export const login = async (username: string, password: string) => {
    const user = await getUserByUsername(username);
    if (!user) return;

    if (!bcript.compareSync(password, user.password))
        return

    const jwtToken = buildJwt(user);
    const token = {
        token: jwtToken,
        email: user.email
    }
    return token;
}

export const register = async (username: string, password: string, email: string) => {
    let user = await getUserByUsername(username);
    if (user) return

    user = await createNewUser(username, password, undefined, email);

    return buildJwt(user);;
}