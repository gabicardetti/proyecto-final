import OrderRepository from "../repositorys/orderRepository";
import { OrderDto, OrderStatus } from "../types/Order";
import { sendNewOrder } from "./emailService";
import { getShoppingCartByUserId, markShoppingCartAsClosed } from "./shoppingCartService";
import { getUserById } from "./userService";

const repository = new OrderRepository();

export const generateOrder = async (userId: string) => {
  const shoppingCart = await getShoppingCartByUserId(userId);
  const user = await getUserById(userId);
  if (!user) return;

  const orderDto: OrderDto = {
    userId,
    shoppingCartId: shoppingCart.id,
    state: OrderStatus.GENERATED,
    date: new Date().toISOString(),
    email: user.email
  }

  const order = await repository.save(orderDto);

  if (!order) return;

  markShoppingCartAsClosed(userId);
  sendNewOrder(order.id, order.email);

  return order;
};

export const getAllByUser = async (userId: string) => {
  return repository.getAll(userId);
}
