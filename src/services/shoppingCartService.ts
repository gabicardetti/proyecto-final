import { getProductById } from "./productService";
import { Product } from "../types/Product";

import { ShoppingCart } from "../types/ShoppingCart"
import ShoppingCartRepositoryMongo from "../repositorys/shoppingCartRepository";

const shoppingRepository = new ShoppingCartRepositoryMongo();


export const getAllProductsInShoppingCart = async (userId: string): Promise<Product[] | undefined> => {
    const shoppingCart = await shoppingRepository.get(userId);
    return shoppingCart ? shoppingCart.products : shoppingCart;
};

export const getProductInShoppingCartById = async (userId: string, productId: string): Promise<Product | undefined> => {
    const shoppingCart = await shoppingRepository.get(userId);
    if (!shoppingCart) return undefined; // should be shopping cart not found
    const product: Product | undefined = shoppingCart.products.find((p) => p.id == productId);
    return product;
};

export const deleteProduct = async (userId: string, productId: string): Promise<ShoppingCart | undefined> => {
    const sucess = await shoppingRepository.deleteProduct(userId, productId);
    if (!sucess || sucess.nModified == 0) return;
    return await shoppingRepository.get(userId);
};

export const addNewProduct = async (userId: string, productId: string): Promise<ShoppingCart | undefined> => {
    const product: Product | undefined = await getProductById(productId);

    if (!product) return; // TODO: return error product not found
    const shoppingCart = await shoppingRepository.get(userId);
    if (!shoppingCart) return;

    shoppingCart.products.push(product);

    const update = await shoppingRepository.updateById(userId, shoppingCart);

    if (!update) return;

    return shoppingCart;
};

export const markShoppingCartAsClosed = async (userId: string) => {
    const shoppingCart: ShoppingCart = await shoppingRepository.get(userId);
    shoppingCart.state = "closed";
    shoppingRepository.updateById(userId, shoppingCart);
}


export const getShoppingCartByUserId = async (userId: string): Promise<ShoppingCart> => {
    const shoppingCart = await shoppingRepository.get(userId);
    return shoppingCart;
}