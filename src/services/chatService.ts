import ChatRepository from "../repositorys/chatRepository";
import { Message, MessageDto, MessageType } from "../types/Message";
// import { sendMessageBySms } from "../services/smsService.js";

const repository = new ChatRepository();

export const saveNewMessage = async (msg: MessageDto) => {
  msg.type = parseType(msg.type);
  return repository.save(msg);
};

export const getAllMessage = async (): Promise<Message[]> => {
  const chatHistory = await repository.getAll();
  return chatHistory;
};

export const getAllMessageFromUser = async (userId: string, email: string): Promise<Message[]> => {
  const chatHistory = await repository.getAllFromUser(userId, email);
  return chatHistory;
};

const parseType = (type: string): MessageType => {
  let parsedType = MessageType.SYSTEM;

  // if (type.toLowerCase() == "user")
  //   parsedType = MessageType.USER;

  return parsedType;
}