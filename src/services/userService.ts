import UserRepositoryMongo from "../repositorys/userRepository";
import bcrypt from "bcryptjs"
import { User, UserDto, UserRole } from "../types/User";

const repository = new UserRepositoryMongo();

export const createNewUser = async (
  username: string,
  password: string,
  profilePhoto: string | undefined,
  email: string) => {
  const hashedPassword = bcrypt.hashSync(password, 10);

  const user: UserDto = {
    username,
    password: hashedPassword,
    role: UserRole.USER,
    email
  }

  if (profilePhoto)
    user.profilePhoto = profilePhoto

  const savedUser = await repository.save(user);
  return savedUser;
};

export const getUserByUsername = async (username: string): Promise<User | undefined> => {
  const user = await repository.getByUsername(username);
  return user;
};

export const getUserById = async (id: string): Promise<User | undefined> => {
  const user = await repository.getById(id);
  return user;
};
