import { NextFunction, Request, Response } from "express";
import * as OrderService from "../services/orderService";
import HttpException from "../types/HttpException";

export const generateOrder = async (req: Request, res: Response, next: NextFunction) => {
    const userId = res.locals.userId;

    const order = await OrderService.generateOrder(userId);

    if (order) res.send({ order });
    else next(new HttpException(400, "problems generating the order"));
}


export const getAllByUser = async (req: Request, res: Response) => {
    const userId = res.locals.userId;

    const orders = await OrderService.getAllByUser(userId);

    res.send({ orders });
}