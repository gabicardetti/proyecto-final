import * as AuthService from "../services/authService";
import { NextFunction, Request, Response } from "express";
import HttpException from "../types/HttpException";

export const login = async (req: Request, res: Response, next: NextFunction) => {
    const { username, password } = req.body;

    const token = await AuthService.login(username, password);

    if (token) {
        req.session.data = {
            email: token.email,
            username
        };
        res.send(token)
    } else next(new HttpException(401, "Not authorized"));
}
export const me = (req: Request & any, res: Response, next: NextFunction) => {
    res.send({ name: req.session.data.username, email: req.session.data.email });
}

export const logout = (req: Request & any, res: Response, next: NextFunction) => {

    req.session.destroy(async (err: any) => {
        if (!err) res.send("Log out ok");
        else next(new HttpException(400, "Log out fail", err));

    });
}

export const register = async (req: Request, res: Response, next: NextFunction) => {
    const { username, password, email } = req.body;

    if (!username || !password || !email) {
        next(new HttpException(400, "username, password and email are requiered"));
        return;
    }

    const token = await AuthService.register(username, password, email);
    if (token) res.send();
    else next(new HttpException(400, "The user already exists"));

}

export const badLogin = (_req: Request, res: Response) => {
    res.redirect("/badLogin.html");

}

export const badRegister = (_req: Request, res: Response) => {
    res.redirect("/badRegister.html");
}