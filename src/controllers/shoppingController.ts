import { NextFunction, Request, Response } from "express";
import { Product } from "../types/Product";
import { ShoppingCart } from "../types/ShoppingCart";
import { getAllProductsInShoppingCart, getProductInShoppingCartById, addNewProduct, deleteProduct } from "../services/shoppingCartService";
import HttpException from "../types/HttpException";

export const getAll = async (req: Request, res: Response, next: NextFunction) => {
    /**
     * get all products from shopping cart
     */

    const userId = res.locals.userId;
    const products: Product[] | undefined = await getAllProductsInShoppingCart(userId);

    if (products && products.length > 0) {
        res.send({ products });
    } else {
        next(new HttpException(400, "No products uploaded yet"));
    }
}

export const getById = async (req: Request, res: Response, next: NextFunction) => {
    /**
     * get a product from the shoppin cart
     */
    const userId = res.locals.userId;
    const productId = req.params.id;
    const products: Product | undefined = await getProductInShoppingCartById(userId ,productId);

    if (products) {
        res.send({ products });
    } else {
        next(new HttpException(400, "Product not found"));
    }
}

export const addNewProductToShoppingCard = async (req: Request, res: Response, next: NextFunction) => {
    /**
     * Add a new product to the shopping cart
     */
    const userId = res.locals.userId;
    const productId = req.params.id;
    const shoppingCart: ShoppingCart | undefined = await addNewProduct(userId, productId);

    if (shoppingCart) {
        res.send();
    } else {
        next(new HttpException(400, "Product not found"));
    }
}

export const deleteProductById = async (req: Request, res: Response, next: NextFunction) => {
    /**
     * Delete a product from the shopping cart
     */
    const userId = res.locals.userId;
    const productId = req.params.id;
    const shoppingCart: ShoppingCart | undefined = await deleteProduct(userId, productId);

    if (shoppingCart) {
        res.send();
    } else {
        next(new HttpException(400, "Product not found"));
    }
}
