import { Request, Response, NextFunction } from "express";
import * as ProductService from "../services/productService";
import { NewProductDto, Product, EditProductDto } from "../types/Product";
import HttpException from "../types/HttpException";

export const getAllProducts = async (_req: Request, res: Response, next: NextFunction) => {
    const products: Product[] = await ProductService.getAllProducts();
    if (products.length > 0) {
        res.send({ products });
    } else {
        next(new HttpException(400, "No products loaded"));
    }
}

export const getProductById = async (req: Request, res: Response, next: NextFunction) => {
    const productId = req.params.id;
    const product: Product | undefined = await ProductService.getProductById(productId);

    if (product) {
        res.send({ product });
    } else {
        next(new HttpException(400, "Product not found"));
    }
}

export const createNewProduct = async (req: Request, res: Response, next: NextFunction) => {
    //TODO: validate body
    const newProductDto = req.body as NewProductDto;

    const product: Product = await ProductService.generateNewProduct(newProductDto);
    if (product) {
        res.send(product);
        const io = req.app.locals.io;
        io.emit("new product", product);
    } else {
        next(new HttpException(400, "Error creating the product"));
    }
}


export const updateProductById = async (req: Request, res: Response, next: NextFunction) => {
    const productId = req.params.id;

    const editProductDto = req.body as EditProductDto;
    const product = await ProductService.updateProductById(productId, editProductDto);

    if (product) {
        res.send();
    } else {
        next(new HttpException(400, "Product not found"));
    }
}

export const deleteProductById = async (req: Request, res: Response, next: NextFunction) => {
    const productId = req.params.id;
    const product = await ProductService.deleteProductById(productId);

    if (product) {
        res.send();
    } else {
        next(new HttpException(400, "Product not found"));
    }
}
