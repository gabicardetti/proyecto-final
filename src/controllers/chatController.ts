import { NextFunction, Request, Response } from "express";
import HttpException from "../types/HttpException";
import * as ChatService from "../services/chatService";
import { MessageDto } from "../types/Message";
export const create = async (req: Request, res: Response, next: NextFunction) => {
    const { email, message, type } = req.body;

    if (!email || !message ) {
        next(new HttpException(400, " email, message and type are required"));
        return;
    }

    const userId = res.locals.userId;

    const dto: MessageDto = {
        date: new Date().toISOString(),
        email,
        message,
        userId,
        type
    }
    const savedMessage = await ChatService.saveNewMessage(dto);
    if (savedMessage) {
        res.send();
        const io = req.app.locals.io;
        io.emit("new message", savedMessage);
    } else next(new HttpException(400));
}

export const getAllMessageFromUser = async (req: Request, res: Response, next: NextFunction) => {
    const userId = res.locals.userId;
    const email = req.params.email;

    if (!email) {
        next(new HttpException(400, " email is required"));
        return;
    }
    const mesages = await ChatService.getAllMessageFromUser(userId, email);

    res.send({ mesages });
}