import dotenv from 'dotenv';
import path from 'path';

dotenv.config({
    path: path.resolve(process.cwd(), process.env.NODE_ENV + '.env')
});


const emailPort = process.env.EMAIL_PORT ? Number(process.env.EMAIL_PORT) : 587;
const config: Config = {
    PORT: Number(process.env.PORT) || 8080,
    JWT_SECRET: process.env.JWT_SECRET || "algo dificil",
    MONGO_URL: process.env.MONGO_URL || "mongodb+srv://testUser:testPassword@cluster0.soy6d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
    EMAIL_HOST: process.env.EMAIL_HOST || "",
    EMAIL_PORT: emailPort,
    EMAIL_AUTH: {
        USER: process.env.EMAIL_AUTH_USER || "",
        PASS: process.env.EMAIL_AUTH_PASS || ""
    },
    TWILIO_AUTH: {
        ACCOUNT_SID: process.env.TWILIO_ACCOUNT_SID || "",
        AUTH_TOKEN: process.env.TWILIO_AUTH_TOKEN || "",
        FROM: process.env.TWILIO_FROM || ""
    },
    SMS_SERVICE_ENABLE: false,
    CHECK_ROLE_ENABLE: false
};

export default config;