import { productSchema } from "./Product";
import mongoose from "mongoose";

const schema = new mongoose.Schema({
    timestamp: {
        type: String,
        required: true
    },
    products: [productSchema],
    userId: {
        type: String,
        required: true
    },
    state: { // can be CLOSED or OPEN
        type: String,
        required: true
    }
});

export const ShoppingCartMongo = mongoose.model("ShoppingCart", schema);