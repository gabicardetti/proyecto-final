import mongoose from "mongoose"
import { UserRole } from "../types/User";

const schema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String
    },
    password: {
        type: String,
        required: true

    },
    profilePhoto: {
        type: String
    },
    userRole: {
        type: String,
        enum: Object.values(UserRole),
        default: UserRole.USER
    }
});

export default mongoose.model("User", schema);