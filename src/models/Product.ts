import mongoose, { Document } from "mongoose"

export const productSchema = new mongoose.Schema({
    timestamp: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    photoUrl: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true

    },
    stock: {
        type: Number,
    }
});

export const ProductMongo = mongoose.model("Product", productSchema);