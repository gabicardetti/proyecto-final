import mongoose from "mongoose";
import { OrderStatus } from "../types/Order";

const schema = new mongoose.Schema({
    date: {
        type: String,
        default: new Date().toISOString()
    },
    userId: {
        type: String,
        required: true
    },
    shoppingCartId: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true,
        enum: Object.values(OrderStatus)
    },
    email: {
        type: String
    }
});

export const OrderMongo = mongoose.model("Order", schema);