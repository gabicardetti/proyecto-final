import mongoose from "mongoose"
import { MessageType } from "../types/Message";

const schema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    date: {
        type: String,
        default: new Date().toISOString()
    },
    userId: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: Object.values(MessageType),
        default: MessageType.SYSTEM
    }
});

export default mongoose.model("Message", schema);
