import UserMongo from "../models/User";
import mongoose from "mongoose";

import { User, UserDto, UserRole } from "../types/User";

export default class UserRepositoryMongo {

    constructor() {
    }

    async save(dto: UserDto): Promise<User> {
        const user = new UserMongo({
            username: dto.username,
            password: dto.password,
            email: dto.email,
            role: dto.role
        });

        return this.parseUser(await user.save());
    }

    async getById(id: string): Promise<User | undefined> {

        const mongoId = mongoose.Types.ObjectId(id);
        const user = await UserMongo.findOne({ _id: { $eq: mongoId } });

        if (!user) return;

        return this.parseUser(user);
    }

    async getByUsername(username: string): Promise<User | undefined> {
        const user = await UserMongo.findOne({ username: { $eq: username } });
        if (!user) return;

        return this.parseUser(user);
    }

    private parseUser = (user: any): User => {
        const parsedUser: User = {
            id: user._id,
            username: user.username,
            email: user.email,
            password: user.password,
            profilePhoto: user.profilePhoto,
            role: this.resolveRole(String(user.role))
        }
        return parsedUser;
    }

    private resolveRole = (role: string): UserRole => {
        let parsedRole = UserRole.USER;

        if (role.toLowerCase() == "admin")
            parsedRole = UserRole.ADMIN;

        return parsedRole;
    }
}