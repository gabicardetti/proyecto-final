import { Product } from "../types/Product";
import { ShoppingCartMongo } from "../models/ShoppingCart";
import mongoose from "mongoose"
import { ShoppingCart } from "../types/ShoppingCart";

// const defaultShoppingCart: ShoppingCart = {
//     id: 1,
//     timestamp: new Date(),
//     products: []
// };
export default class ShoppingCartRepositoryMongo {
    constructor() {

    }

    async get(userId: string) {
        const s = await ShoppingCartMongo.findOne({ state: "OPEN", userId: userId });
        if (!s) return this.create(userId);
        return this.parseShoppingCart(s);

    }

    private async create(userId: string): Promise<ShoppingCart> {
        const shoppingCart = new ShoppingCartMongo({
            timestamp: new Date().toISOString,
            products: [],
            state: "OPEN",
            userId
        });

        return this.parseShoppingCart(await shoppingCart.save());
    }

    async deleteProduct(userId: string, productId: string) {
        try {
            const mongoProductId = mongoose.Types.ObjectId(productId);

            const p = await ShoppingCartMongo.updateOne({ state: "OPEN", userId: userId },
                { "$pull": { "products": { "_id": mongoProductId } } });
            return p;
        } catch (e) {
            return undefined;
        }
    }

    async updateById(userId: string, object: any) {
        let shoppingCart;
        try {
            shoppingCart = await ShoppingCartMongo.updateOne({ state: "OPEN", userId: userId }, object);
        } catch (e) {
            return;
        }

        return shoppingCart;
    }

    private parseShoppingCart = (shoppingCart: any): ShoppingCart => {
        const parsedProduct: ShoppingCart = {
            id: shoppingCart.id,
            timestamp: shoppingCart.timestamp,
            products: shoppingCart.products.map((p: any) => {
                const product: Product = {
                    id: p.id,
                    timestamp: p.timestamp,
                    name: p.name,
                    description: p.description,
                    code: p.code,
                    photoUrl: p.photoUrl,
                    price: p.price,
                    stock: p.stock
                }
                return product;
            }),
            state: shoppingCart.state
        }
        return parsedProduct;
    }

}