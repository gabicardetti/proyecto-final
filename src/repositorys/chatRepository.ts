import { Document } from "mongoose";
import MessageModel from "../models/Message.js"
import { Message, MessageDto } from "../types/Message.js";

export default class ChatRepository {

    constructor() {
    }

    async getAll(): Promise<Message[]> {
        const messages = await MessageModel.find();
        return this.parseMessages(messages);
    }

    async getAllFromUser(userId: string, email: string): Promise<Message[]> {
        const messages = await MessageModel.find({ userId: userId, email: email });
        return this.parseMessages(messages);
    }

    async save(dto: MessageDto): Promise<Message> {
        const message = new MessageModel({
            email: dto.email,
            date: dto.date,
            message: dto.message,
            type: dto.type,
            userId: dto.userId
        });

        return this.parseMessage(await message.save());
    }

    private parseMessages(documents: Document<any, {}>[]): Message[] {
        const result = [];
        for (const document of documents) {
            result.push(this.parseMessage(document));
        }
        return result;
    }

    private parseMessage(data: Document<any, {}>): Message {
        const message: Message = {
            id: data.get("_id"),
            email: data.get("email"),
            date: data.get("date"),
            message: data.get("message"),
            type: data.get("type")
        };

        return message;
    }
}

