import { Document } from "mongoose";
import { OrderMongo } from "../models/Order";
import { Order, OrderDto } from "../types/Order";

export default class OrderRepository {

    constructor() {
    }

    async getAll(userId: string): Promise<Order[]> {
        const orders = await OrderMongo.find({ userId: userId });
        return this.parseOrders(orders);
    }

    async save(dto: OrderDto): Promise<Order> {
        const order = new OrderMongo({
            date: dto.date,
            state: dto.state,
            userId: dto.userId,
            shoppingCartId: dto.shoppingCartId,
            email: dto.email,
        });
        return this.parseOrder(await order.save());
    }

    private parseOrders(documents: Document<any, {}>[]): Order[] {
        const result = [];
        for (const document of documents) {
            result.push(this.parseOrder(document));
        }
        return result;
    }

    private parseOrder(data: Document<any, {}>): Order {
        const order: Order = {
            id: data.get('_id'),
            date: data.get('date'),
            state: data.get('state'),
            userId: data.get('userId'),
            shoppingCartId: data.get('shoppingCartId'),
            email: data.get('email')
        };

        return order;
    }
}

