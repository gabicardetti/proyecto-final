import { NextFunction, Request, Response } from "express"
import { getUserById } from "../services/userService";
import HttpException from "../types/HttpException";
import { UserRole } from "../types/User";
import config from "../config";

export const checkRole = (roles: UserRole[]) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        if (!config.CHECK_ROLE_ENABLE) {
            next();
            return;
        }

        const userId = res.locals.userId;
        const user = await getUserById(userId);

        if (!user) {
            next(new HttpException(401, "Method not allowed"));
            return;
        }

        const userRole = user.role;
        const userHasAccess = roles.includes(userRole);

        if (userHasAccess) next();
        else next(new HttpException(401, "Method not allowed"));
    }
}

// const buildErrorMessage = (url: string) => {

//     // url example /api/productos/borrar/33
//     // after split [ '', 'api', 'productos', 'borrar', '33' ]
//     const urlSplit = url.split("/");
//     const route = urlSplit[1] + "/" + urlSplit[2];
//     const method = urlSplit[3];

//     const error = {
//         error: -1,
//         descripcion: `route ${route} metod ${method} not allowed`
//     }

//     return JSON.stringify(error);
// }