import { Request, Response, NextFunction } from "express";
import { getJwtFromHeaders, verifyAndGetPayload } from "../services/jwtService";
import { getUserById } from "../services/userService";
import HttpException from "../types/HttpException";


export const checkJwt = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
  const token: string = getJwtFromHeaders(req);
  const jwtPayload: any = verifyAndGetPayload(token);

  if (!jwtPayload) {
    next(new HttpException(401, "Not authorized"))
    return;
  }

  const userId = jwtPayload?.userId;
  const user = await getUserById(userId);

  if (!user) {
    next(new HttpException(401, "Not authorized"))
    return;
  }
  res.locals.jwtPayload = jwtPayload;
  res.locals.userId = userId;
  next();
};
