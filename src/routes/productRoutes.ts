const baseProduct = "/product";
import express, { Router } from "express";
import { checkRole } from "../mid/checkRole";

import {
    getAllProducts,
    getProductById,
    createNewProduct,
    updateProductById,
    deleteProductById
} from "../controllers/productController"
import { checkJwt } from "../mid/checkJwt";
import { UserRole } from "../types/User";

const { ADMIN, USER } = UserRole;

const routes: Router = express.Router();
routes.use(checkJwt);
routes.get(
    baseProduct,
    [checkRole([ADMIN, USER])],
    getAllProducts
);

routes.get(
    baseProduct + "/:id",
    [checkRole([ADMIN, USER])],
    getProductById
);

routes.post(baseProduct,
    [checkRole([ADMIN])],
    createNewProduct
);

routes.put(baseProduct + "/:id",
    [checkRole([ADMIN])],
    updateProductById
);

routes.delete(baseProduct + "/:id",
    [checkRole([ADMIN])],
    deleteProductById
);

export default routes;