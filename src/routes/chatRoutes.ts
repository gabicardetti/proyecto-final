const base = "/chat";
import express from "express";
import { create, getAllMessageFromUser } from "../controllers/chatController";

import { checkJwt } from "../mid/checkJwt";

const routes = express.Router();

routes.use(checkJwt);
routes.get(base + "/:email", getAllMessageFromUser);

routes.post(base, create);



export default routes;