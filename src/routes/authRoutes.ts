const base = "/auth";
import express from "express";
import {
    login,
    logout,
    register,
    badRegister,
    badLogin,
    me
} from "../controllers/authController.js"
import { checkJwt } from "../mid/checkJwt.js";

const routes = express.Router();

routes.post(base + "/login", login);
routes.post(base + "/logout", logout);
routes.post(base + "/register", register);
routes.get(base + "/badRegister", badRegister);
routes.get(base + "/badLogin", badLogin);

routes.get("/me", [checkJwt], me);


export default routes;