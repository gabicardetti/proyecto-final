const base = "/order";
import express from "express";
import { getAllByUser, generateOrder } from "../controllers/orderController";

import { checkJwt } from "../mid/checkJwt";
import { checkRole } from "../mid/checkRole";
import { UserRole } from "../types/User";

const routes = express.Router();

const { ADMIN, USER } = UserRole;


routes.use(checkJwt);

routes.get(
    base,
    [checkRole([ADMIN, USER])],
    getAllByUser
);

routes.post(
    base,
    [checkRole([ADMIN, USER])],
    generateOrder);

export default routes;