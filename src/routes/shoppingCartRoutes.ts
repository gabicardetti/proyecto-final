const base = "/cart";
import express, { Router } from "express";
import { checkRole } from "../mid/checkRole";
import {
    getAll,
    getById,
    deleteProductById,
    addNewProductToShoppingCard
} from "../controllers/shoppingController"
import { checkJwt } from "../mid/checkJwt";
import { UserRole } from "../types/User";

const { ADMIN, USER } = UserRole;


const routes: Router = express.Router();
routes.use(checkJwt);

routes.get(
    base + "/product",
    [checkRole([ADMIN, USER])],
    getAll
);

routes.get(
    base + "/product/:id",
    [checkRole([ADMIN, USER])],
    getById
);

routes.post(
    base + "/product/:id",
    [checkRole([ADMIN, USER])],
    addNewProductToShoppingCard
);

routes.delete(
    base + "/product/:id",
    [checkRole([ADMIN, USER])],
    deleteProductById
);


export default routes;