import express, { Application } from "express";

import ProductRoutes from "./routes/productRoutes";
import ShoppingCartRoutes from "./routes/shoppingCartRoutes";
import AuthRoutes from "./routes/authRoutes";
import ChatRoutes from "./routes/chatRoutes";
import OrderRoutes from "./routes/orderRoutes";



import mongoose from "mongoose";
import config from "./config";

import MongoStore from "connect-mongo";
import session from "express-session";
import logger from "./util/logger";
import errorHandler from "./mid/errorHandler";
import http from "http";
import { Server, Socket} from "socket.io";
import { getAllProducts } from "./services/productService";

declare module 'express-session' {
    interface SessionData {
        data: UserSession;
    }
}

const port: number = config.PORT;
const app: Application = express();

const server = http.createServer(app);
const io = new Server(server, {});
app.locals.io = io;
const baseUrl: string = "/api";
const mongoUrl: string = config.MONGO_URL;

app.use(express.static("public"));



app.use(session({
    secret: "secret",
    resave: false,
    saveUninitialized: false,
    store: new MongoStore(
        {
            mongoUrl,
            mongoOptions: {
                useUnifiedTopology: true
            }
        }
    )
}));

app.use(express.json());
app.use(baseUrl, AuthRoutes);
app.use(baseUrl, ProductRoutes);
app.use(baseUrl, ShoppingCartRoutes);
app.use(baseUrl, AuthRoutes);
app.use(baseUrl, ChatRoutes);
app.use(baseUrl, OrderRoutes);


app.use(errorHandler);



(async () => {
    try {
        await mongoose.connect(
            mongoUrl,
            {
                useFindAndModify: false,
                useUnifiedTopology: true
            })
    } catch (e) {
        console.error(e)
        throw new Error("mongo db cannot be connected");
    }

    io.on("connection", async (socket: Socket) => {
        logger.info("a user connected");

        const products = await getAllProducts();
        socket.emit("initialization", { products });

        // socket.on("new message", (msg) => {
        //   io.emit("messages", msg);
        //   saveNewMessage(msg);

        // });

    });
    server.listen(port, () => {
        logger.info("running on port " + port);
    })

})()
