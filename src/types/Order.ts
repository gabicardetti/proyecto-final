export enum OrderStatus {
    GENERATED = "generated",
    DELIVERED = "delivered"
}

export interface Order {
    id: string
    date: string
    state: string
    userId: string
    shoppingCartId: string
    email: string
}


export interface OrderDto {
    date: string
    state: string
    userId: string
    shoppingCartId: string
    email: string
}
