export enum UserRole {
    ADMIN = 'admin',
    USER = 'user'
};


export interface UserDto {
    username: string,
    password: string,
    email?: string,
    profilePhoto?: string
    role: UserRole
}

export interface User {
    id: string,
    username: string,
    email: string,
    password: string,
    profilePhoto?: string,
    role: UserRole
}