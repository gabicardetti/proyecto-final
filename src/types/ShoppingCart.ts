import { Product } from "./Product";

export interface ShoppingCart {
    id: string,
    timestamp: Date,
    products: Product[],
    state: string
}