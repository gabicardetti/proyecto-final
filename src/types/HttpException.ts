class HttpException extends Error {
    status: number;
    message: string;
    error: any;
    constructor(status: number, message?: string, error?: any) {
        super(message);
        this.status = status;
        this.message = message || "error";
        this.error = error;
    }
}

export default HttpException;