export interface Message {
    id: string
    date: string
    email: string
    message: string
    type: string | MessageType
}

export interface MessageDto {
    date: string
    email: string
    message: string
    userId: string
    type: string | MessageType
}

export enum MessageType {
    USER = "user",
    SYSTEM = "system"
}