interface EmailAuth {
    USER: string
    PASS: string
}

interface TwilioAuth {
    ACCOUNT_SID: string
    AUTH_TOKEN: string
    FROM: string
}


interface Config {
    PORT: number
    JWT_SECRET: string
    MONGO_URL: string
    EMAIL_HOST: string
    EMAIL_PORT: number
    SMS_SERVICE_ENABLE: boolean
    TWILIO_AUTH: TwilioAuth
    EMAIL_AUTH: EmailAuth,
    CHECK_ROLE_ENABLE: boolean
}
