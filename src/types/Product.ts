export interface Product {
    id?: string,
    timestamp: Date,
    name: string,
    description: string,
    code: string,
    photoUrl: string,
    price: number,
    stock: number,
}

export interface EditProductDto {
    timestamp?: Date,
    name?: string,
    description?: string,
    code?: string,
    photoUrl?: string,
    price?: number,
    stock?: number,
}

export interface NewProductDto {
    name: string,
    description: string,
    code: string,
    photoUrl: string,
    price: number,
    stock: number,
}