function formSubmit(event) {
  event.preventDefault();
  const name = document.getElementById("fName").value;
  const description = document.getElementById("fDescription").value;
  const code = document.getElementById("fCode").value;
  const stock = document.getElementById("fStock").value;
  const price = document.getElementById("fPrice").value;
  const photoUrl = document.getElementById("fPhotoUrl").value;

  const body = {
    name,
    description,
    code,
    stock,
    price,
    photoUrl,
  };

  const url = "/api/product";
  fetch(url, {
    method: "post",
    body: JSON.stringify(body),
    headers: headersC,
  })
    .then(function (response) {
      if (response.status == 401) {
        alert("metodo no autorizado");
      } else return response.json();
    })
    .then(function (data) {
      document.getElementById("fName").value = "";
      document.getElementById("fDescription").value = "";
      document.getElementById("fCode").value = "";
      document.getElementById("fStock").value = "";
      document.getElementById("fPrice").value = "";
      document.getElementById("fPhotoUrl").value = "";
      alert("Producto creado con exito");
    });
}
document.getElementById("form").addEventListener("submit", formSubmit);
