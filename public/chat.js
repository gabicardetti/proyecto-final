const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};
const formMessage = document.getElementById("formMessage");
const inputMessage = document.getElementById("inputMessage");
const inputEmail = document.getElementById("inputEmail");


formMessage.addEventListener("submit", function (e) {
  e.preventDefault();
  const email = inputEmail.value;
  if (!validateEmail(email)) {
    alert("email no valido");
    return;
  }
  if (!inputMessage.value) return;
  const message = {
    date: new Date(),
    email,
    message: inputMessage.value,
  };
  sendMessage(message);
});

socket.on("new message", (msg) => {
  console.log(msg, "wee")
  let newMessage = "";
  newMessage += ` <span class="email"> ${msg.email} </span>`
  newMessage += ` <span class="date"> [${msg.date}]:  </span>`
  newMessage += ` <span class="message" > ${msg.message} </span>`


  document.getElementById("messageBody").innerHTML += newMessage + "<br>";
});


const sendMessage = (message) => {
  console.log("hola2")

  const url = "/api/chat";
  fetch(url, {
    method: 'post',
    body: JSON.stringify(message),
    headers: headersC
  }).then(function (response) {
    if (response.status == 401) {
      alert("metodo no autorizado");
    } 
    inputMessage.value = "";
  }).catch((error) => {
    console.log(error)
  });
}