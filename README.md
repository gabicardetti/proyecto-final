# Eccomerce

_Proyecto final de curso Backend en CODER HOUSE_

## Requisitos

_Dependencias necesarias para poder correr el proyecto correctamente_

``` bash
node --version
v14.15.4

yarn --version
1.22.10

```

## Instalación

``` bash
git clone https://gitlab.com/gabicardetti/proyecto-final
cd PROJECT
yarn
```

## Configuracion de variables de entorno

_Lo primero que tenemos que hacer es crear un archivo llamado **(entorno).env** en la carpeta de raiz, tomando como ejemplo el archivo example.env:_

``` env
PORT=
JWT_SECRET=
MONGO_URL=

EMAIL_HOST=
EMAIL_PORT=

EMAIL_AUTH_USER=
EMAIL_AUTH_PASS=

```

las cuales son todas necesarias para el correcto funcionameinto de la aplicacion.
Para generar las credenciales necesarias y probar el envio de emails cuando una orden es generada
entrar a https://ethereal.email/    

## Ejecutando el proyecto

Para poder correr el proyecto, solo debemos ir a la carpeta de raiz del proyecto y correr

``` bash
yarn start (esto por defecto levanta el archivo local.env)
yarn start:dev (esto por defecto levanta el archivo develop.env)
yarn start:prod (esto por defecto levanta el archivo productiom.env)
```

Luego en la terminal nos dice en que direccion esta hosteado el proyecto donde por defecto es **localhost:8080**

### Postman

Dejo la collecion de postman y varibales de entornos para probar la api, es importante tener las variables de entorno creadas ya que todos los endpoints llevan una token de autorizacion y con esta collecion de postman una vez que nos logeamos el token se setea en las variables de entorno y luego cada enpoint lo consume desde ahi, esto hace mas facil el testeo.

Las variables de entorno son

baseUrl: "localhost:8080"
token: "" (este se actualiza automaticante cuando te logeas)

## Construido con

_Estas fueron algunas de las herramientas se utilizaron para crear el proyecto_

* [Express ](https://reactjs.org/)
* [Mongoose ](https://developers.google.com/maps/documentation/)

## Autores ✒️

* **Gabriel Cardetti** 

## Detalles del proyecto

### Login

Seguridad en los enpoints, por defecto la variables para ser administrador o no esta apagado, para prender el checkeo de roles tenemos que ir a la config y setear CHECK_ROLE_ENABLE = true y para acceder a los enpoint protegidos tenemos que crear un usuario (por defecto este usario tiene role USER) y tenemos que cambiar por base de datos al role ADMIN.

## Notas

Dejo un frontend minimo armado, el cual implementa un login, una vista de productos, carga de productos y el chat conectado por websockets.

Tambien dejo por defecto una db subida en Mongo Atlas para probar
